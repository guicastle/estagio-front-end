﻿HTML:

(Disposição dos elementos)
Elements Block level
Elements Inline level

Quando utilizamos, por exemplo, o “display:block” nós estamos dizendo ao HTML que aquele elemento específico vai ser renderizado. Nós queremos que o usuário possa identificar sua posição, cor, altura, largura e etc.Um ótimo exemplo de “block” é a tag <div>. Ela já possui atributo “display:block” em sua estrutura.

O “display:inline” desloca seus elementos para uma posição horizontal, colocando-os um ao lado do outro, mas faz com que o elemento perca algumas das características “block”, como havia falado mais a cima, ou seja, ele não se comporta mais como uma forma consistente, perde altura, largura e outros atributos mais. Para adicionar volume a um elemento com este atributo, devemos acrescentar “padding” ao mesmo.

(Construção da macação)
Acessibilidade
Semantica
Usabilidade

(Renderização)
Flow
Reflow

CSS

Seletores
States(hover focus active visited)
Pseudo-seletores
Propriedades
Valores
Media queries

estilo estatico
estilo estado

1- MEXER NA POSIÇÃO DOS ELEMENTOS 
2- MEXER NA ALTURA E LARGURA
3- MEXER No ESTILO